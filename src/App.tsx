import { useState } from "react";
import "./App.scss";
import pierre from "./assets/pierre.png";
import feuille from "./assets/feuille.png";
import ciseaux from "./assets/ciseaux.png";
import PlayAction from "./components/PlayActions";
import Header from "./components/Header";
import { IActionType } from "./types";
import {  rules } from "./utils";
import LastResult from "./components/LastResult";
import GameResult from "./components/GameResult";

const actions: IActionType[] = [
  { name: "pierre", image: pierre },
  { name: "feuille", image: feuille },
  { name: "ciseaux", image: ciseaux },
];

function App() {
  const [scoreUser, setScoreUser] = useState<number>(0);
  const [scoreComputer, setScoreComputer] = useState<number>(0);
  const [actionUser, setActionUser] = useState<string>("");
  const [actionComputer, setActionComputer] = useState<string>("");
  const [lastResult, setLastResult] = useState<string[]>([]);

  function userPlayed(actionName: string) {
    //Random computer play
    const randomComputerPlay: string =
      actions[Math.floor(Math.random() * actions.length)].name;

    //Stock game score
    setActionUser(actionName);
    setActionComputer(randomComputerPlay);
    let result: string = rules(actionName, randomComputerPlay);

    //Increment player score
    if (result === "user") {
      setScoreUser(scoreUser + 1);
    } else if (result === "computer") {
      setScoreComputer(scoreComputer + 1);
    }

    //Stock Result
    let newResult = lastResult.concat(result);
    setLastResult(newResult);
  }

  function reset(){
    setLastResult([])
    setScoreUser(0)
    setScoreComputer(0)
    setActionUser('')
    setActionComputer('')
  }

  return (
    <div className="global-container">
      <Header scoreUser={scoreUser} scoreComputer={scoreComputer} reset={reset}/>
      <PlayAction type="computer" action={actionComputer} />
      <GameResult lastMatch={lastResult[lastResult.length-1]}/>
      <PlayAction type="user" action={actionUser} userPlayed={userPlayed} />
      <LastResult lastResult={lastResult} />
    </div>
  );
}

export default App;
