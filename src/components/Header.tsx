import { IHeader } from "../types";
import "./../App.scss";
import replay from "./../assets/replay.png";


function Header({ scoreUser, scoreComputer, reset }: IHeader) {

  
  return (
    <header className="header">
      <h1>SHIFUMI</h1>
      <p className="score">
        {`User : ${scoreUser} - Computer : ${scoreComputer}`}{" "}
      </p>
      <img alt='reset' src={replay} width={20} onClick={reset}/>
    </header>
  );
}

export default Header;
