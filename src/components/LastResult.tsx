import { useEffect } from "react";
import "./../App.scss";
import { convertGameResultToString } from "../utils";
import { ILastResult } from "../types";

function LastResult({ lastResult }: ILastResult) {

useEffect(()=>{
  var scroll=document.getElementById("result-container-scroll");
  if(scroll){scroll.scrollTop = scroll?.scrollHeight;
  }
},[lastResult])


  return lastResult.length ?  (
    <div className="last-result-global-container">
      <h2>Resultat : </h2>
      <div id='result-container-scroll'>
      {lastResult.map((r: string) => (
        <p>
          {convertGameResultToString(r)}
        </p>
      ))}
      </div>
    </div>
  ): null;
}

export default LastResult;
