import "./../App.scss";
import pierre from "./../assets/pierre.png";
import feuille from "./../assets/feuille.png";
import ciseaux from "./../assets/ciseaux.png";
import { IActionType, IPlayAction } from "../types";

const actions: IActionType[] = [
  { name: "pierre", image: pierre },
  { name: "feuille", image: feuille },
  { name: "ciseaux", image: ciseaux },
];

function PlayAction({ type, action, userPlayed }: IPlayAction) {
  function handlePlayAction(a: { name: string; image: string }) {
    if (userPlayed) userPlayed(a.name);
  }
  return (
    <div className={`${type}-player`}>
      {actions.map((a) => (
        <img
          alt={a.name}
          src={a.image}
          className={action === a.name ? `is-played-${type}` : ""}
          width={200}
          onClick={() => handlePlayAction(a)}
        />
      ))}
    </div>
  );
}

export default PlayAction;
