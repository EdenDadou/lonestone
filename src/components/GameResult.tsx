import "../App.scss";
import { convertGameResultToString, } from "./../utils";
import { IGameResult } from "../types";

function GameResult({lastMatch}:IGameResult ) {

  return (
    <div className="game-result-container">
      <span >{convertGameResultToString(lastMatch)}</span>
    </div>
  );
}

export default GameResult;
