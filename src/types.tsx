export interface IActionType {
  name: string;
  image: string;
}

export interface ILastResult {
  lastResult: string[];
}
export interface IGameResult {
  lastMatch: string;
}

export interface IHeader {
  scoreUser: number;
  scoreComputer: number;
  reset: ()=>void
}

export interface IPlayAction {
  type: string;
  action: string;
  userPlayed?: (action: string) => void;
}
