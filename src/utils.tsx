export function rules(userPlayed: string, computerPlayed: string) {
  switch (userPlayed) {
    case "pierre":
      return computerPlayed === "ciseaux"
        ? "user"
        : computerPlayed === "feuille"
        ? "computer"
        : "nul";
    case "feuille":
      return computerPlayed === "pierre"
        ? "user"
        : computerPlayed === "ciseaux"
        ? "computer"
        : "nul";
    case "ciseaux":
      return computerPlayed === "feuille"
        ? "user"
        : computerPlayed === "pierre"
        ? "computer"
        : "nul";
    default:
      return "nul";
  }
}

export function convertGameResultToString(value : string){
  if(!value) return
  return value !== "nul"
  ? value === 'user' ?  "You Win !" : "You Lose !" 
  : "Draw"
}
